import { Dish } from './../../models/dish.model';
import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})

export class ModalComponent {

  @Input() dish: Dish;
  @Output() closeEvent = new EventEmitter<any>();

  constructor() {}

  public closeModal(): void {
    this.closeEvent.emit();
  }
}
