import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() {}

  login(name: string, password: string): void {
    if (password === '123456') {
      localStorage.setItem('islogin', 'true');
      localStorage.setItem('username', name);
    }
  }

  logout(): void {
    localStorage.removeItem('islogin');
    localStorage.removeItem('username');
  }

  get name(): string {
    const username = localStorage.getItem('username');
    if (username) {
      return username;
    }
    return null;
  }

  get isLogin(): boolean {
    const islogin = localStorage.getItem('islogin');
    if (islogin) {
      return true;
    }
    return false;
  }
}
