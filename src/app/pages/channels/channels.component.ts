import { DataService } from './../../services/data.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-channels-page',
  templateUrl: './channels.component.html',
  styleUrls: ['./channels.component.scss']
})

export class ChannelsPageComponent {



  constructor(private dataService: DataService) {}


  public get summ(): number {
    return this.dataService.totalSumm;
  }
}
