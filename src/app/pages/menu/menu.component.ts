import { Dish } from './../../models/dish.model';
import { Component } from '@angular/core';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuPageComponent {
  public dishes: Dish[] = [
    {
      id: 0,
      name: 'Драники',
      description: '',
      picture: '../assets/draniki.jpg',
      price: 2.1
    },
    {
      id: 1,
      name: 'Лазанья',
      description: '',
      picture: '../assets/lazania.jpg',
      price: 1.6
    },
    {
      id: 2,
      name: 'Пицца',
      description: '',
      picture: '../assets/pizza.jpg',
      price: 9.8
    },
    {
      id: 3,
      name: 'Рыба',
      description: '',
      picture: '../assets/fish.jpg',
      price: 5.5
    },
    {
      id: 4,
      name: 'Стейк с картофелем',
      description: '',
      picture: '../assets/steik.jpg',
      price: 8.6
    }
  ];

  public order: Dish[] = [];

  public isShowModal: boolean;
  public modalData: Dish;

  constructor() {}

  public onSelect(id: number) {
    const item: Dish = this.dishes.find(dish => dish.id === id);
    this.order.push(item);
  }

  public get isHaveOrder(): boolean {
    return this.order.length > 0;
  }

  public showDescription(dish: Dish): void {
    this.modalData = dish;
    this.isShowModal = true;
  }

  public onModalClose(): void {
    this.isShowModal = false;
    this.modalData = null;
  }

  public onDeleteItem(item: Dish): void {
    const index = this.order.indexOf(item);
    this.order.splice(index, 1);
  }
}
