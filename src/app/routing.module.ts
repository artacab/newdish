import { LoginPageComponent } from './pages/login/login.component';
import { ChannelsPageComponent } from './pages/channels/channels.component';
import { MenuPageComponent } from './pages/menu/menu.component';
import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';
import { AuthGuard } from './services/auth.guard';
import { LoginGuard } from './services/login.guard';


const routes: Route[] = [
  {
    path: '',
    redirectTo: 'menu',
    pathMatch: 'full'
  },
  {
    path: 'menu',
    component: MenuPageComponent
  },
  {
    path: 'channels',
    component: ChannelsPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginPageComponent,
    canActivate: [LoginGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})

export class RoutingModule {}
